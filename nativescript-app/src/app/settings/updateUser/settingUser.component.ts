import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import * as localStorage from "nativescript-localstorage";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "UpdateUser",
    moduleId: module.id,
    template: `
        <ActionBar>
            <Label text="Update username"></Label>
        </ActionBar>

        <StackLayout>
            <TextField
                [(ngModel)]="textFieldUser"
                hint="Username"
                required
            ></TextField>
            <Button text="Save" (tap)="onButtonTap()"></Button>
        </StackLayout>
    `
})
export class UpdateUserComponent implements OnInit {
    textFieldUser: string;

    constructor(private _routerExtensions: RouterExtensions) {}

    ngOnInit() {
        this.textFieldUser = localStorage.getItem("user");
    }

    onButtonTap() {
        localStorage.setItem("user", this.textFieldUser);
        this.changePage();
    }

    changePage() {
        this._routerExtensions.navigate(["settings"], {
            transition: {
                name: "fade"
            }
        });
    }
}
