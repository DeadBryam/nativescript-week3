import { Routes } from "@angular/router";
import { IndicatorComponent } from "./indicator.component";
import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [{ path: "", component: IndicatorComponent }];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class IndicatorRoutingModule {}
