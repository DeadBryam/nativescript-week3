import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { IndicatorRoutingModule } from "./indicator-routing.module";
import { IndicatorComponent } from "./indicator.component";

@NgModule({
    imports: [NativeScriptCommonModule, IndicatorRoutingModule],
    declarations: [IndicatorComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class IndicatorModule {}
