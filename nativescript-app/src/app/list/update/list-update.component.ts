import { Component, Input, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { ListadoService } from "~/app/domain/listado.service";

@Component({
    selector: "UpdateModal",
    templateUrl: "list-update.component.html"
})
export class UpdateModalComponent implements OnInit {
    randomNumber: string;
    index: number;

    constructor(
        private params: ModalDialogParams,
        private listado: ListadoService
    ) {}

    update() {
        // this.listado.update(this.index, this.randomNumber);
        this.params.closeCallback();
    }

    ngOnInit() {
        this.randomNumber = this.params.context.value;
        // this.index = this.listado.WfindAll().indexOf(this.randomNumber);
    }
}
