import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
let sqlite = require("nativescript-sqlite");

@Injectable()
export class ListadoService {
    private listado: Array<String> = [];
    private puntuaciones: Array<Puntuacion> = [];
    api: string = " https://d09d3516.ngrok.io";

    constructor() {
        this.getDB(
            db => {
                console.dir(db);
                db.each(
                    "select * from favs",
                    (err, fila) => console.log("fila", fila),
                    (err, totales) => console.log("total", totales)
                );
            },
            _ => {
                console.log("error on getDB");
            }
        );
    }

    findByName(s: string) {
        return getJSON(`${this.api}/get?q=${s}`);
    }

    //puntuaciones

    addRandomPuntuacion(): void {
        this.puntuaciones.push({
            imagen: "res://icon",
            nombre: "Random",
            puntuacion: (Math.random() * 100).toFixed()
        });
    }

    findAllPuntuaciones() {
        return this.puntuaciones;
    }

    deletePuntuacion(index: number): void {
        this.puntuaciones.splice(index, 1);
    }

    //sqlite

    getDB(fnOk, fnError) {
        return new sqlite("fav_db", (err, db) => {
            err
                ? console.log("error al abrir db")
                : (console.log("la db fue abierta"),
                  db
                      .execSQL(
                          "CREATE TABLE IF NOT EXISTS favs (id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT)"
                      )
                      .then(
                          id => {
                              console.log("create table ok");
                              fnOk(db);
                          },
                          error => {
                              console.log("create table error");
                              fnError(error);
                          }
                      ));
        });
    }

    //favoritos

    addToFavs(s: string) {
        this.getDB(
            db => {
                db.execSQL(
                    "insert into favs (nombre) values (?)",
                    [s],
                    (err, id) => console.log("nuevo id ", id)
                );
            },
            _ => console.log("error on getDB")
        );
    }

    getFavs():Array<string> {
        let favs: Array<string> = [];
        this.getDB(
            db => {
                db.each("select nombre from favs", (err, item) => {
                    favs.push(item[0]);
                });
                return favs;
            },
            _ => console.log("error on getDB")
        );

        return favs;
    }
}

export class Puntuacion {
    imagen: String;
    nombre: String;
    puntuacion: String;
}
